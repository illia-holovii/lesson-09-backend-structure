import React from 'react';
import PropTypes from 'prop-types';
import { Card, Label, Icon, Comment as CommentUI } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = (props, post, likePost, disLikePost) => {
    const { comment: { body, createdAt, user } } = props;
    const { likeCount } = post;
    const date = moment(createdAt).fromNow();
    return (
        <CommentUI className={styles.comment}>
            <CommentUI.Avatar src={getUserImgLink(user.image)} />
            <CommentUI.Content>
                <CommentUI.Author as="a">
                    {user.username}
                </CommentUI.Author>
                <CommentUI.Metadata>
                    {date}
                </CommentUI.Metadata>
                <CommentUI.Text>
                    {body}
                </CommentUI.Text>
                <Card.Content extra>
                    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(user.id)}>
                        <Icon name="thumbs up" />
                        {likeCount}
                    </Label>
                    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => disLikePost(user.id)}>
                        <Icon name="thumbs down" />
                        {likeCount}
                    </Label>
                </Card.Content>
            </CommentUI.Content>
        </CommentUI>
    );
};

Comment.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    likePost: PropTypes.func.isRequired,
    disLikePost: PropTypes.func.isRequired,
    comment: PropTypes.objectOf(PropTypes.any).isRequired
};

export default Comment;
